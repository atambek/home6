import jdk.nashorn.internal.ir.WhileNode;

import java.util.*;

/**ÜL 2. Koostada meetod, mis teeb kindlaks,
 * kas etteantud sidusas lihtgraafis leidub Euleri tsükkel,
 * ning selle leidumisel nummerdab kõik servad vastavalt nende järjekorrale Euleri tsükli läbimisel
 * (vt. ka Fleury' algoritm).
 */

/**
 * Fleury's algorithm for finding Euler path or cycle according to https://www.geeksforgeeks.org/fleurys-algorithm-for-printing-eulerian-path/
 * 1. Make sure the graph has either 0 or 2 odd vertices.
 * 2. If there are 0 odd vertices, start anywhere. If there are 2 odd vertices, start at one of them.
 * My remark: Euler cycle possible only in case no odd-degree vertices exist
 * 3. Follow edges one at a time. If you have a choice between a bridge and a non-bridge, always choose the non-bridge.
 * 4. Stop when you run out of edges.
 * <p>
 * The idea is, “don’t burn bridges“
 * so that we can come back to a vertex
 * and traverse remaining edges.
 */



/**
 * Class used to select next edge while traversing Eulerian cycle. Program selects the edge which leads to vertex with max edges.
 * Not the best solution, but no more brain potential available at the moment.
 * */
class VertexEdge {
    public GraphTask.Vertex vertex;
    public int numberOfEdges;
    public GraphTask.Arc a;
    public VertexEdge(GraphTask.Vertex v, int number, GraphTask.Arc a){
        this.vertex = v;
        this.numberOfEdges = number;
        this.a = a;
    }
}

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {
    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
        //throw new RuntimeException ("Nothing implemented yet!"); // delete this
    }
    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createTestGraph5();
        System.out.println(g);
        g.executeFleuryAlgorithm();
    }
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        //ANT >>
        public void setInfo(int i){
            info = i;
        }
        // ANT <<

        // TODO!!! Your Vertex methods here!
    }
    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        //ANT >>
        public Vertex from;
        public int sequence = 0;
        public boolean removed = false;
        public Arc equivilent;
        //ANT <<

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        // TODO!!! Your Arc methods here!
    }

    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        //int[][] matrix = null;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }


        /**
         * Method for creating duplicate edges for i-j --> j-i.
         * Necessary to check if odd degree vertices exist in graph
         * @param aid   --> id
         * @param from  -->
         * @param to
         * @param e
         * @return
         */
        public Arc createArc2(String aid, Vertex from, Vertex to, Arc e) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            res.equivilent = e;
            e.equivilent = res;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        // TODO!!! Your Graph methods here! Probably your solution belongs here.


        /**
         * Checks if conditions for Eulerian cycle are met and initiates traversion
         */
        public void executeFleuryAlgorithm() {
            // Condition1 : Check if odd vertices exist
            if (countOddVertices() > 0)
                throw new RuntimeException("Graph contains odd-degree vertices. Eulerian cycle doesn't exist");

            //reset vertex and arc info
            resetVertexInfo();
            resetArcInfo();

            //start tour
            traverseEulerianCycle(first,0);
        }

        /**
         * Method to find route for traversing Eulerian cycle
         *
         * @param v - marks the beginning vertex
         * @param stepCounter - counts steps and sets arc info
         */
        public void traverseEulerianCycle(Vertex v, int stepCounter){
            resetVertexInfo();
            Stack<Vertex> vertexStack = new Stack<>();
            vertexStack.push(v);
            v.setInfo(1);
            while (!vertexStack.empty()){
                v = vertexStack.pop();
                v.setInfo(2);
                stepCounter++;
                Arc a = v.first;
                Arc e = a;

                //pick the edge that leads to the vertex with max number of available edges
                ArrayList<VertexEdge> vertexEdges = new ArrayList<>();
                while (e != null){
                    if (!e.removed) {
                        Vertex w = e.target;
                        Arc c = w.first;
                        int vertexCounter = 0;
                        while (c != null) {
                            if (!c.removed)
                                vertexCounter++;
                            c = c.next;
                        }
                        VertexEdge vertexEdge = new VertexEdge(w, vertexCounter, e);
                        vertexEdges.add(vertexEdge);
                    }
                    e = e.next;
                }

                VertexEdge max = new VertexEdge(null,0,null);
                for (int i = 0; i < vertexEdges.size(); i++) {
                    if (max.numberOfEdges <= vertexEdges.get(i).numberOfEdges)
                        max = vertexEdges.get(i);
                }


                if (max.numberOfEdges > 0) {
                    a = max.a;
                    a.removed = true;
                    a.equivilent.removed = true;
                    a.sequence = stepCounter;
                    a.equivilent.sequence = stepCounter;
                    System.out.println(a.id);
                    vertexStack.push(max.vertex);
                    max.vertex.setInfo(1);
                }
            }
        }

        /**
         * Method to check if odd degree vertices exist in graph.
         * Eulerian cycle only possible if no odd degree vertices exist
         *
         * @return - retirns 1 if exist, otherwize 0
         */
        public int countOddVertices() {
            int[][] matrix = createAdjMatrix();
            int v = countVertices(); //number of vertices
            if (v == 0) throw
                new RuntimeException("No vertices found!");
            int e; // number of edges connected to vertex
            boolean edgeExist = false;
            for (int row = 0; row < matrix.length; row++) {
                e = 0;
                for (int col = 0; col < matrix[row].length; col++) {
                    if (matrix[row][col] == 1)
                        e++;
                }
                if (e % 2 == 1) return 1; //odd degree vertex exists, exit loop
            }
            return 0;
        }


        /**
         * Method to count the number of vertices in graph
         *
         * @return
         */
        public int countVertices() {
            int counter = 0;
            Vertex v = first;
            while (v != null) {
                counter++;
                v = v.next;
            }
            return counter;
        }

        /**
         * Method to reset arc-related information
         *
         */
        public void resetArcInfo(){
            Arc a = first.first;
            while (a != null){
                a.info = 0;
                a.equivilent.info = 0;
                a.sequence = 0;
                a.equivilent.sequence = 0;
                a.removed = false;
                a.equivilent.removed = false;
                a = a.next;
            }
        }

        /**
         * Method to reset arc-related information
         */
        public void resetVertexInfo(){
            Vertex w = first;
            while (w != null){
                w.info = 0;
                w = w.next;
            }
        }

        /**
         * Methods for graph creation for testing purposes
         */
        public void createTestGraph1() {
            Vertex[] varray = new Vertex[5];
            for (int i = 0; i < 5; i++) {
                varray[i] = createVertex("v" + String.valueOf(5-i));
            }

            createArc2("a" + varray[0].toString() + "_"
                            + varray[1].toString(), varray[0], varray[1],
                    createArc("a" + varray[1].toString() + "_"
                            + varray[0].toString(), varray[1], varray[0])); //1-2

            createArc2("a" + varray[0].toString() + "_"
                            + varray[2].toString(), varray[0], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[0].toString(), varray[2], varray[0])); //1-3

            createArc2("a" + varray[1].toString() + "_"
                            + varray[2].toString(), varray[1], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[1].toString(), varray[2], varray[1])); //2-3

            createArc2("a" + varray[2].toString() + "_"
                            + varray[3].toString(), varray[2], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[2].toString(), varray[3], varray[2])); //3-4

            createArc2("a" + varray[2].toString() + "_"
                            + varray[4].toString(), varray[2], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[2].toString(), varray[4], varray[2])); //3-5

            createArc2("a" + varray[3].toString() + "_"
                            + varray[4].toString(), varray[3], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[3].toString(), varray[4], varray[3])); // 4-5
        }

        public void createTestGraph2() {
            Vertex[] varray = new Vertex[4];
            for (int i = 0; i < 4; i++) {
                varray[i] = createVertex("v" + String.valueOf( i + 1));
            }

            createArc2("a" + varray[0].toString() + "_"
                            + varray[1].toString(), varray[0], varray[1],
                    createArc("a" + varray[1].toString() + "_"
                            + varray[0].toString(), varray[1], varray[0])); //1-2


            createArc2("a" + varray[1].toString() + "_"
                            + varray[2].toString(), varray[1], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[1].toString(), varray[2], varray[1])); //2-3

            createArc2("a" + varray[0].toString() + "_"
                            + varray[3].toString(), varray[0], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[0].toString(), varray[3], varray[0])); //1-4

            createArc2("a" + varray[1].toString() + "_"
                            + varray[3].toString(), varray[1], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[1].toString(), varray[3], varray[1])); //2-4

            createArc2("a" + varray[2].toString() + "_"
                            + varray[3].toString(), varray[2], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[2].toString(), varray[3], varray[2])); //3-4

        }

        public void createTestGraph3() {
            Vertex[] varray = new Vertex[6];
            for (int i = 0; i < 6; i++) {
                varray[i] = createVertex("v" + String.valueOf(6 - i));
            }

            createArc2("a" + varray[0].toString() + "_"
                            + varray[1].toString(), varray[0], varray[1],
                    createArc("a" + varray[1].toString() + "_"
                            + varray[0].toString(), varray[1], varray[0])); //1-2

            createArc2("a" + varray[0].toString() + "_"
                            + varray[5].toString(), varray[0], varray[5],
                    createArc("a" + varray[5].toString() + "_"
                            + varray[0].toString(), varray[5], varray[0])); //1-6

            createArc2("a" + varray[0].toString() + "_"
                            + varray[4].toString(), varray[0], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[0].toString(), varray[4], varray[0])); //1-5

            createArc2("a" + varray[1].toString() + "_"
                            + varray[2].toString(), varray[1], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[1].toString(), varray[2], varray[1])); //2-3

            createArc2("a" + varray[1].toString() + "_"
                            + varray[5].toString(), varray[1], varray[5],
                    createArc("a" + varray[5].toString() + "_"
                            + varray[1].toString(), varray[5], varray[1])); //2-6

            createArc2("a" + varray[2].toString() + "_"
                            + varray[3].toString(), varray[2], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[2].toString(), varray[3], varray[2])); //3-4

            createArc2("a" + varray[2].toString() + "_"
                            + varray[4].toString(), varray[2], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[2].toString(), varray[4], varray[2])); //3-5

            createArc2("a" + varray[2].toString() + "_"
                            + varray[5].toString(), varray[2], varray[5],
                    createArc("a" + varray[5].toString() + "_"
                            + varray[2].toString(), varray[5], varray[2])); //3-6

            createArc2("a" + varray[3].toString() + "_"
                            + varray[4].toString(), varray[3], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[3].toString(), varray[4], varray[3])); // 4-5

            createArc2("a" + varray[4].toString() + "_"
                            + varray[5].toString(), varray[4], varray[5],
                    createArc("a" + varray[5].toString() + "_"
                            + varray[4].toString(), varray[5], varray[4])); // 5-6


        }

        public void createTestGraph4() {
            Vertex[] varray = new Vertex[4];
            for (int i = 0; i < 4; i++) {
                varray[i] = createVertex("v" + String.valueOf(i + 1));
            }

            createArc2("a" + varray[0].toString() + "_"
                            + varray[1].toString(), varray[0], varray[1],
                    createArc("a" + varray[1].toString() + "_"
                            + varray[0].toString(), varray[1], varray[0])); //1-2

            createArc2("a" + varray[0].toString() + "_"
                            + varray[2].toString(), varray[0], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[0].toString(), varray[2], varray[0])); //1-3

            createArc2("a" + varray[1].toString() + "_"
                            + varray[2].toString(), varray[1], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[1].toString(), varray[2], varray[1])); //2-3

            createArc2("a" + varray[2].toString() + "_"
                            + varray[3].toString(), varray[2], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[2].toString(), varray[3], varray[2])); //3-4

        }

        public void createTestGraph5() {
            Vertex[] varray = new Vertex[6];
            for (int i = 0; i < 6; i++) {
                varray[i] = createVertex("v" + String.valueOf(i + 1));
            }

            createArc2("a" + varray[0].toString() + "_"
                            + varray[1].toString(), varray[0], varray[1],
                    createArc("a" + varray[1].toString() + "_"
                            + varray[0].toString(), varray[1], varray[0])); //1-2

            createArc2("a" + varray[0].toString() + "_"
                            + varray[4].toString(), varray[0], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[0].toString(), varray[4], varray[0])); //1-5

            createArc2("a" + varray[0].toString() + "_"
                            + varray[5].toString(), varray[0], varray[5],
                    createArc("a" + varray[5].toString() + "_"
                            + varray[0].toString(), varray[5], varray[0])); //1-6

            createArc2("a" + varray[0].toString() + "_"
                            + varray[2].toString(), varray[0], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[0].toString(), varray[2], varray[0])); //1-3

            createArc2("a" + varray[1].toString() + "_"
                            + varray[2].toString(), varray[1], varray[2],
                    createArc("a" + varray[2].toString() + "_"
                            + varray[1].toString(), varray[2], varray[1])); //2-3


            createArc2("a" + varray[2].toString() + "_"
                            + varray[3].toString(), varray[2], varray[3],
                    createArc("a" + varray[3].toString() + "_"
                            + varray[2].toString(), varray[3], varray[2])); //3-4

            createArc2("a" + varray[2].toString() + "_"
                            + varray[4].toString(), varray[2], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[2].toString(), varray[4], varray[2])); //3-5

            createArc2("a" + varray[3].toString() + "_"
                            + varray[4].toString(), varray[3], varray[4],
                    createArc("a" + varray[4].toString() + "_"
                            + varray[3].toString(), varray[4], varray[3])); // 4-5

            createArc2("a" + varray[4].toString() + "_"
                            + varray[5].toString(), varray[4], varray[5],
                    createArc("a" + varray[5].toString() + "_"
                            + varray[4].toString(), varray[5], varray[4])); //5-6

        }
    }
} 

